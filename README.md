# zz-gatling-jan11-gatling

### Dependencies

* `$ brew install scala`


### Gatling Test Development

https://gatling.io/

Run your app-under-test locally, and configure the baseUrl in `src/gatling/resources/test.conf`

### Test Execution

```shell
  $ ./gradlew gatlingRun
```

### Test Results

Written to console and `zz-gatling-jan11-gatling/build/reports/gatling/*/index.html`


### Load Setup Instructions

load paramteters can be configured in `template/src/gatling/resources/test.conf` file

Current configuration is given below: 

```shell
parallelQueries=100
rampFrom=10
rampTo=100
rampDuration=2
```