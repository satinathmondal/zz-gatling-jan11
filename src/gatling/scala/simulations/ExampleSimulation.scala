package simulations

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration.DurationInt


class ExampleSimulation extends Simulation {
  val baseUrl: String = TestConfiguration.baseUrl

  val httpProtocol: HttpProtocolBuilder = http
    .warmUp(baseUrl + "/actuator/health")
    .baseUrl(baseUrl) // Here is the root for all relative URLs
   //.header("Accept", "application/json")
  
  val scn: ScenarioBuilder = scenario("Root Simulation")
    .exec(http("Root URL")
      .get("/")
      .check(status.in(200))
      //.check(jsonPath("$.hello").exists)
      //.check(jsonPath("$.hello").is("world"))
    )
    //.pause(8)
    //.exec(
    //  http("/api/hello")
     //   .get("/api/hello")
     //   .check(status.in(200))
      //  .headers(headers_1)
      //  .check(jsonPath("$.response").is("Hello World"))
    //)

  setUp(scn
    .inject(
      atOnceUsers(TestConfiguration.parallelQueries),
      rampUsersPerSec(TestConfiguration.rampFrom).
        to(TestConfiguration.rampTo).
        during(TestConfiguration.rampDuration.minutes))
    .protocols(httpProtocol))
}
