package simulations

import com.typesafe.config.{Config, ConfigFactory}

object TestConfiguration {
  val config: Config = ConfigFactory.load("test.conf")

  var baseUrl: String = config.getString("baseUrl")
  var parallelQueries: Int = config.getInt("parallelQueries")
  var rampFrom: Int = config.getInt("rampFrom")
  var rampTo: Int = config.getInt("rampTo")
  var rampDuration: Int = config.getInt("rampDuration")
}
